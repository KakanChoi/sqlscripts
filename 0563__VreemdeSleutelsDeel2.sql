USE Modernways;

SELECT Artiesten.Naam, Liedjes.Titel
FROM Liedjes
CROSS JOIN Artiesten
WHERE Artiesten_Id = Id;
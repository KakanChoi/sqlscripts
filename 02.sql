USE TussentijdseEvaluatie;
SET SQL_SAFE_UPDATES = 0;
UPDATE boeken SET Commentaar = 'Geen vrolijk boek'
	WHERE JaarVanUitgave > 1938 AND JaarVanUitGave < 1946;
SET SQL_SAFE_UPDATES = 1;

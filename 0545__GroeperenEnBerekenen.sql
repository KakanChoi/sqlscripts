USE modernways;
SELECT Artiest, SUM(Aantalbeluisteringen)
FROM liedjes
WHERE Length(Artiest) >= 10
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) > 100

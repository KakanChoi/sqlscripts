USE ModernWays;

ALTER TABLE Liedjes
ADD COLUMN Artiesten_Id INT,
ADD CONSTRAINT fk_Liedjes_Artiesten
  FOREIGN KEY (Artiesten_Id)
  REFERENCES Artiesten(Id);
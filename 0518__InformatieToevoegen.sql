USE ModernWays;
ALTER TABLE huisdieren ADD COLUMN Geluid VARCHAR(20) CHAR SET utf8mb4;
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Geluid = 'WAF!' WHERE Soort = 'hond';
UPDATE huisdieren SET Geluid = 'miauwww...!' WHERE Soort = 'kat';
SET SQL_SAFE_UPDATES = 1;
SELECT * FROM huisdieren
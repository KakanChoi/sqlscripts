USE ModernWays;
ALTER TABLE huisdieren ADD COLUMN Omschrijving VARCHAR(150) CHAR SET utf8mb4;
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Omschrijving = concat(Naam, ' de ', Soort);
SET SQL_SAFE_UPDATES = 1;
SELECT * FROM huisdieren;
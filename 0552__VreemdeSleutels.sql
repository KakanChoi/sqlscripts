USE ModernWays;

ALTER TABLE Baasjes
ADD COLUMN Huisdieren_Id INT,
ADD CONSTRAINT fk_Baasjes_Huisdieren
  FOREIGN KEY (Huisdieren_Id)
  REFERENCES Huisdieren(Id);
  
-- ALTER TABLE Baasjes ADD COLUMN Huisdieren_Id VARCHAR(150) CHAR SET utf8mb4;
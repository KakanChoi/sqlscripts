USE TussentijdseEvaluatie;
INSERT INTO personen (
Voornaam, Familienaam, Geboortedatum, GSM_Nummer
)
VALUES (
('Dolan', 'Mitchell', 1998-12-07, 0106175893),
('Nicholas', 'Stafford', 1907-09-17, 0568274065),
('Graham', 'Wilkerson', 1967-08-20, 0119148762),
('Dylan', 'Rodriguez', 1978-04-16, 0319287874),
('Colt', 'Hardy', 1972-06-15, 0950393235),
('Henry', 'Vasquez', 2003-04-27, 0171624603)
);

select * from personen
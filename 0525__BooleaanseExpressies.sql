USE modernways;
/*ALTER TABLE huisdieren ADD COLUMN Bewering VARCHAR(150) CHAR SET utf8mb4;*/
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Bewering = concat(Naam, ' is de naam van een hond')
where Soort = 'hond';
SET SQL_SAFE_UPDATES = 1;
select*from huisdieren;
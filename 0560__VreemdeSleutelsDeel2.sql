USE Modernways;
/*
INSERT INTO Liedjes (
Artiesten_Id
)
VALUES
(1),
(1),
(2),
(3),
(4),
(5),
(5),
(5),
(6),
(7);*/

SET SQL_SAFE_UPDATES = 0;

UPDATE Liedjes SET Artiesten_Id = 1
WHERE Artiest = 'Led Zeppelin';

UPDATE Liedjes SET Artiesten_Id = 2
WHERE Artiest = 'The Doors';

UPDATE Liedjes SET Artiesten_Id = 3
WHERE Artiest = 'Molly Tuttle';

UPDATE Liedjes SET Artiesten_Id = 4
WHERE Artiest = 'Goodnight, Texas';

UPDATE Liedjes SET Artiesten_Id = 5
WHERE Artiest = 'Layla Zoe';

UPDATE Liedjes SET Artiesten_Id = 6
WHERE Artiest = 'Danielle Nicole';

UPDATE Liedjes SET Artiesten_Id = 7
WHERE Artiest = 'Van Halen';

SET SQL_SAFE_UPDATES = 1;